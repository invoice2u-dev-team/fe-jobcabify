package org.invoice2u;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import org.apache.tomcat.util.codec.binary.Base64;
import org.invoice2u.constant.Constantes;
import org.invoice2u.model.Respuesta;
import org.invoice2u.util.C2UResponseErrorHandler;
import org.invoice2u.util.LoggingRequestInterceptor;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Component
public class Scheduler {

    private static final Logger LOG = LoggerFactory.getLogger(Scheduler.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private static final String API_CABIFY_URL = "api.url.fe-cabify";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Value("${email.server}")
    private String emailServer;

    @Value("${email.port}")
    private Integer emailPort;

    @Value("${email.user}")
    private String emailUser;

    @Value("${email.password}")
    private String emailPassword;

    private String buscarConfiguracion(String id) {
        String sql = "SELECT valor FROM invoice2udb.configuracion WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, String.class);
    }

    private RestTemplate buildRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ClientHttpRequestInterceptor ri = new LoggingRequestInterceptor();
        List<ClientHttpRequestInterceptor> ris = new ArrayList<>();
        ris.add(ri);
        restTemplate.setInterceptors(ris);
        restTemplate.setErrorHandler(new C2UResponseErrorHandler());
        return restTemplate;
    }

    private HttpHeaders buildCabifyHeaders() {
        String usuario = "diego.heredia@cabify.com";
        String clave = "$$dheredia";
        //String ruc = "20548704261";
        String auth = usuario + ":" + clave;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8")));
        String authHeader = "Basic " + new String(encodedAuth);

        HttpHeaders result = new HttpHeaders() {{
            set("Authorization", authHeader);
        }};
        result.set("Accept", "application/json");
        result.set("X-Auth-Token", "2054870426120548704261$$20548704261");

        return result;
    }

    @Scheduled(cron = "0 0 0/1 * * *")
    public void rrhhCabifyScheduler() {
        LOG.info(" -- ROBOT Inicio --");

        try {
            String url = buscarConfiguracion(API_CABIFY_URL) + "/secure/descargarRhh";
            LOG.info(" api url is " + url);
            RestTemplate restTemplate = buildRestTemplate();
            HttpHeaders header = buildCabifyHeaders();
            ConsultarRhhRequest entity = new ConsultarRhhRequest();
            DateTime dt = new DateTime();
            if (dt.getHourOfDay() < 10) {
                entity.setFechaEmisionInicio(dt.minusDays(2).toDate());
            } else {
                entity.setFechaEmisionInicio(dt.toDate());
            }
            entity.setFechaEmisionFin(dt.toDate());
            ObjectMapper mapper = new ObjectMapper();
            LOG.info(url + " -> " + mapper.writeValueAsString(entity));
            HttpEntity<ConsultarRhhRequest> request = new HttpEntity<>(entity, header);
            ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, request, Respuesta.class);
            Respuesta result = (Respuesta) response.getBody();
            LOG.info(" -- ROBOT Fin -- ");
        } catch (Exception e) {
            LOG.error("error al ejecutar tarea ", e);

            if (getServidor().contains("invoice2u")) {
                String mensaje = "fe-jobcabify\n";
                mensaje = mensaje + "Throwable ";
                mensaje = mensaje + Throwables.getStackTraceAsString(e) + "\n";

                String subject = "Incidente en fe-jobcabify @ " + getServidor();
                String fromStr = buscarConfiguracion(Constantes.SMTP_FROM);
                String toStr = buscarConfiguracion(Constantes.SMTP_TO);

                enviarCorreo(fromStr, toStr, subject, mensaje);
            }
        }
    }

    @Scheduled(cron = "0 0/3 * * * *")
    public void tickerReporteScheduler() {
        LOG.info(" -- ticket reporte Inicio --");

        //String url = "https://invoice2u.pe/fe-cabify" + "/secure/procesar";

        try {
            String url = buscarConfiguracion(API_CABIFY_URL) + "/secure/procesar";
            LOG.info(" api url is " + url);
            RestTemplate restTemplate = buildRestTemplate();
            HttpHeaders header = buildCabifyHeaders();
            TicketRequest ticketRequest = new TicketRequest();
            HttpEntity<TicketRequest> request = new HttpEntity<>(ticketRequest, header);
            ResponseEntity response = restTemplate.postForEntity(url, request, Void.class);
            Respuesta result = (Respuesta) response.getBody();
            LOG.info(" -- ROBOT Fin -- ");

        } catch (Exception e) {
            LOG.error("error al ejecutar tarea ", e);

            if (getServidor().contains("invoice2u")) {
                String mensaje = "fe-jobcabify\n";
                mensaje = mensaje + "Throwable ";
                mensaje = mensaje + Throwables.getStackTraceAsString(e) + "\n";

                String subject = "Incidente en fe-jobcabify @ " + getServidor();
                String fromStr = buscarConfiguracion(Constantes.SMTP_FROM);
                String toStr = buscarConfiguracion(Constantes.SMTP_TO);

                enviarCorreo(fromStr, toStr, subject, mensaje);
            }
        }
    }

    private void enviarCorreo(String fromStr, String toStr, String subject, String mensaje) {
        Properties props = System.getProperties();

        props.put("mail.smtp.host", emailServer);
        props.put("mail.smtp.port", emailPort); //TLS Port
        props.put("mail.smtp.auth", "true"); //enable authentication
        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

        //create Authenticator object to pass in Session.getInstance argument
        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailUser, emailPassword);
            }
        };

        Session session = Session.getInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);

        try {
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.setFrom(fromStr);
            msg.setSubject(subject, "UTF-8");
            msg.setText(mensaje, "UTF-8");
            msg.setSentDate(new Date());
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toStr, false));
            Transport.send(msg);
            LOG.info("Envio de correo exitoso!");
        } catch (MessagingException e) {
            LOG.error("Error al enviar excepcion por correo ", e);
        }
    }

    private String getServidor() {
        try {
            String servidor = InetAddress.getLocalHost().getHostName();
            LOG.debug("El servidor es: " + servidor);
            return servidor;
        } catch (java.net.UnknownHostException ex) {
            return ex.getMessage();
        }
    }
}