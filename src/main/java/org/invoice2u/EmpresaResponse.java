package org.invoice2u;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EmpresaResponse {
    @JsonProperty("ruc")
    private String ruc = null;
    @JsonProperty("apiKey")
    private String apiKey = null;
}
