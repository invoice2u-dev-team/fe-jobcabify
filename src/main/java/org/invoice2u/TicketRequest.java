package org.invoice2u;

import lombok.Data;

/**
 * Created by Kaiser .
 */
@Data
public class TicketRequest {
    private String ticket;
}
