package org.invoice2u;

import org.apache.tomcat.util.codec.binary.Base64;
import org.invoice2u.entity.Usuario;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.Charset;
import java.util.List;

public class HttpProvider {
    public static EmpresaResponse buscarEmpresa(String url, String ruc) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", "cl@se2u");
        HttpEntity<String> request = new HttpEntity<>(headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<EmpresaResponse> response = restTemplate.exchange(url + "/secure/BuscarEmpresa?ruc=" + ruc, HttpMethod.GET, request, EmpresaResponse.class);
        return response.getBody();
    }

    public static List<Usuario> obtenerUsuarios(String url, String ruc) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", "cl@se2u");
        HttpEntity<String> request = new HttpEntity<>(headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/secure/BuscarUsuario").queryParam("ruc", ruc);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Usuario>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, request, new ParameterizedTypeReference<List<Usuario>>() {});
        return response.getBody();
    }

    public static HttpHeaders buildHeaders(String apikey, Usuario usuario) {
        String auth = usuario.getUsuario() + ":" + usuario.getClave();
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("UTF-8")));
        String authHeader = "Basic " + new String(encodedAuth);

        HttpHeaders result = new HttpHeaders();
        result.add("Authorization", authHeader);
        result.setContentType(MediaType.APPLICATION_JSON);

        result.set("X-Auth-Token", apikey);

        return result;
    }
}
