package org.invoice2u;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ImportResource({"app-context-dao.xml"})
public class Application {

    public static void main(String[] args) throws Exception {
        /*
        new SpringApplicationBuilder()
                .web(WebApplicationType.NONE)
                .sources(Application.class)
                .run(args);
        */
        SpringApplication.run(Application.class);
    }
}