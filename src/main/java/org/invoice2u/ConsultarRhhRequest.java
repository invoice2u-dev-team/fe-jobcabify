package org.invoice2u;

import lombok.Data;

import java.util.Date;

@Data
public class ConsultarRhhRequest {
    private Date fechaEmisionInicio;
    private Date fechaEmisionFin;
}