package org.invoice2u;

import lombok.Data;

/**
 *
 */
@Data
public class MailAuthBean {
    String host;
    String user;
    String password;
    String port;
    String from;
    String to;
    String cc;
    String cco;
}